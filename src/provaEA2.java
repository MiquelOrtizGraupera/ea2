import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.Scanner;

public class provaEA2 {
    public static void main(String[] args) throws IOException {
       // escribirFichAleatorio();
        leerFichAleatorio();
        //leerFichAleatorioUnReg();
        //addFichAleatoriUnReg();
        //modificarFichAleatorio();
}
private static void escribirFichAleatorio() throws IOException{
    File fichero = new File("AleatorioEmple.dat");
    //declara el fichero de acceso aleatorio
    RandomAccessFile file = new RandomAccessFile(fichero, "rw");
    //arrays con los datos
    String apellido[] = {"FERNANDEZ","GIL","LOPEZ","RAMOS",
            "SEVILLA","CASILLA", "REY"};//apellidos
    int dep[] = {10, 20, 10, 10, 30, 30, 20};       //departamentos
    Double salario[]={1000.45, 2400.60, 3000.0, 1500.56,
            2200.0, 1435.87, 2000.0};//salarios

    StringBuffer buffer = null;//buffer para almacenar apellido
    int n=apellido.length;//numero de elementos del array

    for (int i=0;i<n; i++){ //recorro los arrays
        file.writeInt(i+1); //uso i+1 para identificar empleado
        buffer = new StringBuffer( apellido[i] );
        buffer.setLength(10); //10 caracteres para el apellido
        file.writeChars(buffer.toString());//insertar apellido
        file.writeInt(dep[i]);       //insertar departamento
        file.writeDouble(salario[i]);//insertar salario
    }
    file.close();  //cerrar fichero
}

private static void leerFichAleatorio() throws IOException{
    File fichero = new File("AleatorioEmple.dat");
    //declara el fichero de acceso aleatorio
    RandomAccessFile file = new RandomAccessFile(fichero, "r");
    //
    int  id, dep, posicion;
    Double salario;
    char apellido[] = new char[10], aux;

    posicion = 0;  //para situarnos al principio

    for(;;){  //recorro el fichero
        file.seek(posicion); //nos posicionamos en posicion
        id = file.readInt();   // obtengo id de empleado

        //recorro uno a uno los caracteres del apellido
        for (int i = 0; i < apellido.length; i++) {
            aux = file.readChar();
            apellido[i] = aux;    //los voy guardando en el array
        }

        //convierto a String el array
        String apellidos = new String(apellido);
        dep = file.readInt();        //obtengo dep
        salario = file.readDouble(); //obtengo salario

        if(id >0)
            System.out.printf("ID: %s, Apellido: %s, Departamento: %d, Salario: %.2f %n",
                    id,   apellidos.trim(), dep, salario);

        //me posiciono para el sig empleado, cada empleado ocupa 36 bytes
        posicion= posicion + 36;

        //Si he recorrido todos los bytes salgo del for
        if (file.getFilePointer() == file.length())break;

    }//fin bucle for
    file.close();  //cerrar fichero
}

private static void leerFichAleatorioUnReg() throws IOException{
    Scanner lector = new Scanner(System.in);
    System.out.println("Escriu un id:\n");
    int user = lector.nextInt();
    File fichero = new File("AleatorioEmple.dat");
    //declara el fichero de acceso aleatorio
    RandomAccessFile file = new RandomAccessFile(fichero, "r");
    //
    int  id, dep, posicion;
    Double salario;
    char apellido[] = new char[10], aux;
    int registro = Integer.parseInt(String.valueOf(user)); //recoger id de empleado a consultar

    posicion = (registro -1 ) * 36;
    if(posicion >= file.length() )
        System.out.printf("ID: %d, NO EXISTE EMPLEADO...",registro);
    else{
        file.seek(posicion); //nos posicionamos
        id=file.readInt(); // obtengo id de empleado
        for (int i = 0; i < apellido.length; i++) {
            aux = file.readChar();//recorro uno a uno los caracteres del apellido
            apellido[i] = aux;    //los voy guardando en el array
        }
        String apellidoS= new String(apellido);//convierto a String el array
        dep=file.readInt();//obtengo dep
        salario=file.readDouble();  //obtengo salario

        System.out.println("ID: " + registro + ", Apellido: "+
                apellidoS.trim() +
                ", Departamento: "+dep + ", Salario: " + salario);
    }
    file.close();  //cerrar fichero
}
private static void addFichAleatoriUnReg() throws IOException{
    File fichero = new File("AleatorioEmple.dat");
    //declara el fichero de acceso aleatorio
    RandomAccessFile file =
            new RandomAccessFile(fichero, "rw");

    /*StringBuffer buffer = null;	//bufer para almacenar apellido
    String apellido="GONZALEZ";
    Double salario=1230.87;
    int dep=10;

    //INSERTAR UN REGISTRO AL FINAL DEL FICHERO
   /*long posicion= file.length() ;
   file.seek(posicion);
   int id= (int) ((posicion + 36) / 36);
   */
    //insertar reg con id 20

    //int id=20;
    StringBuffer buffer = null;
    String apellido="ORTIZ";
    double salario = 1100.50;
    int dep=30;
    int id=8;
    long posicion = (id -1 ) * 36;
    file.seek(posicion); //nos posicionamos

    file.writeInt(id); //uso id para identificar empleado
    buffer = new StringBuffer( apellido);
    buffer.setLength(10); //10 caracteres para el apellido
    file.writeChars(buffer.toString());//insertar apellido
    file.writeInt(dep);       //insertar departamento
    file.writeDouble(salario);//insertar salario


    file.close();  //cerrar fichero
}
private static void modificarFichAleatorio() throws IOException{
    Path home = Path.of(System.getProperty("user.home"));
    Path fichero = home.resolve("IdeaProjects/ea2/AleatorioEmple.dat");
    File fixer = new File("AleatorioEmple.dat");
    //declara el fichero de acceso aleatorio
    RandomAccessFile file = new RandomAccessFile(fixer, "rw");
    //

    int registro = 20  ;//id a modificar

    long posicion = (registro -1 ) * 36; //(4+20+4+8)  modifico salario y dep
    posicion=posicion+4+20; //sumo el tama�o de ID+apellido
    file.seek(posicion); //nos posicionamos
    file.writeInt(40);   //modif departamento
    file.writeDouble(4000.87);//modif salario
    file.close();  //cerrar fichero
}
}




