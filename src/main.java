import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

public class main {
    public static void main(String[] args) {

        try {
            ex1();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ex2();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ex3();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ex4();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ex5();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void ex1() throws IOException {
        Scanner lector = new Scanner(System.in);
        System.out.println("Escriu l'id per consultar:");
        int user = lector.nextInt();

        File fixer = new File("AleatorioEmple.dat");
        RandomAccessFile acces = new RandomAccessFile(fixer, "rw");

        int idBuscar = user;
        int posicio = (idBuscar -1) * 36;
        char cognom[] = new char[10];

        if(posicio >= fixer.length())
            System.out.println("Id incorrecte, no existeix");
        else{
            acces.seek(posicio); //->Ens posicionem sobre l'element
            int id = acces.readInt(); //-> Obtenim l'id
            for(int i = 0; i < cognom.length; i++){
                char recorregutPerGuardar = acces.readChar();
                cognom[i] = recorregutPerGuardar; //-> Guardem un a un els chars recorreguts
            }
            String cognomFet = new String(cognom); //-> Creo String d'array de chars
            int departament = acces.readInt(); //-> Obtenir departament
            double salari = acces.readDouble();//->Llegir el salari

            System.out.println("Id: "+id+", Cognom: "+cognomFet.trim()+", Departament: "+departament+", salari: "+salari);
        }
        acces.close();
    }

    public static void ex2() throws IOException{
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        File fixer = new File("AleatorioEmple.dat");
        RandomAccessFile acces = new RandomAccessFile(fixer,"rw");

        int posicio = 0;
        System.out.println("Escriu un id:");
        int id = lector.nextInt();
        lector.nextLine();
        while(posicio != acces.length()){
            acces.seek(posicio);
            if(id == acces.readInt()){
                System.out.println("L'id ja existeix");
                return;
            }
            posicio = posicio +36;
        }
        System.out.println("escriu cognom:");
        String cognom = lector.nextLine();
        System.out.println("escriu departament");
        int dep = lector.nextInt();
        lector.nextLine();
        System.out.println("escriu salari:");
        double salari = lector.nextDouble();

        StringBuffer buffer = null;
        int posicioPunter = (id-1)*36;
        acces.seek(posicioPunter);
        acces.writeInt(id);
        buffer = new StringBuffer(cognom);
        buffer.setLength(10);
        acces.writeChars(buffer.toString());
        acces.writeInt(dep);
        acces.writeDouble(salari);
        System.out.println("Insertat amb éxit");
        acces.close();
    }

    private static void ex3() throws  IOException{
    Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        File fitxer = new File("AleatorioEmple.dat");
        RandomAccessFile accesFitxer = new RandomAccessFile(fitxer,"rw");

        //Dades Interactives
        System.out.println("Escriu un id per modificar:");
        int userId = lector.nextInt();
        lector.nextLine();
        System.out.println("Escriu import a sumar al salari:");
        double salariAdd = lector.nextDouble();

        //Funcionalitats per interactuar
        int posicio = 0,id, dep;
        char cognom []= new char[10], aux;
        double salari;
        String cognomFi="";

        while(posicio != accesFitxer.length()){
            accesFitxer.seek(posicio);
            if(userId != accesFitxer.readInt()){
                posicio = posicio + 36;
            }else{
                for(int i = 0; i < cognom.length; i++){
                    aux = accesFitxer.readChar();
                    cognom[i] = aux;
                }
                cognomFi = String.valueOf(cognom);
                accesFitxer.readInt();
                double salariDocu = accesFitxer.readDouble();
                posicio += 24;
                accesFitxer.seek(posicio);
                double salariFi = salariDocu + salariAdd;
                accesFitxer.writeDouble(salariFi);
                System.out.println("Salari incrementat correctament");

                System.out.println("Cognom: "+cognomFi.trim()+" Salari antic: "+salariDocu+" --> Salari nou: "+salariFi);
                break;
            }
        }
        accesFitxer.close();
    }
    private static void ex4() throws IOException{
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        File fitxer = new File("AleatorioEmple.dat");
        RandomAccessFile accesFitxer = new RandomAccessFile(fitxer,"rw");

        System.out.println("Escriu l'id a esborrar");
        int userId = lector.nextInt();
        char cognom[] = new char[10];
        int dep = 0;
        double salari= 0;
        int posicio = 0;
        String cognomFi = String.valueOf(cognom);
        int idChange = -1;

        while(posicio != accesFitxer.length()){
            accesFitxer.seek(posicio);
            if(userId == accesFitxer.readInt()){
                for(int i = 0; i < cognom.length; i++){
                    char borrat ='\u0000';
                    cognom[i] = borrat;
                }
                int depDoc = accesFitxer.readInt();
                double salariDoc = accesFitxer.readDouble();
                posicio = Math.toIntExact(accesFitxer.getFilePointer());
                accesFitxer.seek(posicio);
                posicio -= 36;
                accesFitxer.writeInt(idChange);
                accesFitxer.writeChars(cognomFi);
                accesFitxer.writeInt(dep);
                accesFitxer.writeDouble(salari);
                System.out.println("Id: "+idChange+" Cognom: "+cognomFi+" dep: "+dep+" salari: "+salari);
                break;
            }
            posicio += 36;
        }
        accesFitxer.close();
    }
    private static void ex5() throws IOException{
        File fitxer = new File ("AleatorioEmple.dat");
        RandomAccessFile accesArxiu = new RandomAccessFile(fitxer ,"rw");

        int posicio = 0;

        while (posicio != accesArxiu.length()){
            if(accesArxiu.readInt() == -1){
                System.out.println(accesArxiu.readInt());
            }else{
                posicio +=36;
            }
        }
        accesArxiu.close();
    }

}
